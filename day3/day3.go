package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

// copied from https://stackoverflow.com/a/18479916/13130130
// readLines reads a whole file into memory
// and returns a slice of its lines.
func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		value := scanner.Text()
		lines = append(lines, value)
	}
	return lines, scanner.Err()
}

func handleError(err error) {
	if err != nil {
		log.Fatalf("readLines: %s", err)
	}
}

// https://stackoverflow.com/a/37563128/13130130
func filter(ss []string, test func(string) bool) (ret []string) {
	for _, s := range ss {
		if test(s) {
			ret = append(ret, s)
		}
	}
	return
}

func task1() string {
	lines, err := readLines("./day3/input.txt")

	handleError(err)

	epsilonStr := ""
	gammaStr := ""

	// for each col in the input "table"
	for characterIdx := 0; characterIdx < len(lines[0]); characterIdx++ {
		zeroCount := 0
		oneCount := 0
		// for each row of the column
		for rowIdx := 0; rowIdx < len(lines); rowIdx++ {
			if lines[rowIdx][characterIdx] == '0' {
				zeroCount++
			} else {
				oneCount++
			}
		}
		if oneCount > zeroCount {
			epsilonStr += "1"
			gammaStr += "0"
		} else {
			epsilonStr += "0"
			gammaStr += "1"
		}

	}
	epsilon, err := strconv.ParseInt(epsilonStr, 2, 64)
	gamma, err := strconv.ParseInt(gammaStr, 2, 64)
	handleError(err)
	return fmt.Sprintf("1: Result: %d", epsilon*gamma)
}

func task2() string {
	lines, err := readLines("./day3/input.txt")

	handleError(err)

	oxygenWorkingLines := make([]string, len(lines))
	co2WorkingLines := make([]string, len(lines))
	copy(oxygenWorkingLines, lines)
	copy(co2WorkingLines, lines)

	// for each col in the input "table"
	for characterIdx := 0; characterIdx < len(lines[0]); characterIdx++ {
		zeroCount := 0
		oneCount := 0
		// for each row of the column
		for rowIdx := 0; rowIdx < len(oxygenWorkingLines); rowIdx++ {
			if oxygenWorkingLines[rowIdx][characterIdx] == '0' {
				zeroCount++
			} else {
				oneCount++
			}
		}
		if oneCount >= zeroCount {
			oxygenWorkingLines = filter(oxygenWorkingLines, func(s string) bool { return s[characterIdx] == '1' })
		} else {
			oxygenWorkingLines = filter(oxygenWorkingLines, func(s string) bool { return s[characterIdx] == '0' })
		}
		if len(co2WorkingLines) == 1 {
			break
		}
	}

	for characterIdx := 0; characterIdx < len(lines[0]); characterIdx++ {
		zeroCount := 0
		oneCount := 0
		// for each row of the column
		for rowIdx := 0; rowIdx < len(co2WorkingLines); rowIdx++ {
			if co2WorkingLines[rowIdx][characterIdx] == '0' {
				zeroCount++
			} else {
				oneCount++
			}
		}
		if oneCount < zeroCount {
			co2WorkingLines = filter(co2WorkingLines, func(s string) bool { return s[characterIdx] == '1' })
		} else {
			co2WorkingLines = filter(co2WorkingLines, func(s string) bool { return s[characterIdx] == '0' })
		}
		if len(co2WorkingLines) == 1 {
			break
		}
	}
	o2, err := strconv.ParseInt(oxygenWorkingLines[0], 2, 64)
	co2, err := strconv.ParseInt(co2WorkingLines[0], 2, 64)
	handleError(err)
	return fmt.Sprintf("2: Result: %d", o2*co2)
}

func main() {
	println(task1())
	println(task2())
}
