package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

// copied from https://stackoverflow.com/a/18479916/13130130
// readLines reads a whole file into memory
// and returns a slice of its lines.
func readLines(path string) ([]int, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []int
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		value, err := strconv.Atoi(scanner.Text())
		handleError(err)
		lines = append(lines, value)
	}
	return lines, scanner.Err()
}

func handleError(err error) {
	if err != nil {
		log.Fatalf("readLines: %s", err)
	}
}

func task1() string {
	lines, err := readLines("./day1/input.txt")

	handleError(err)
	increaseCount := 0
	lastNumber := 0

	for i := 0; i < len(lines)-1; i++ {
		if lines[i] > lastNumber {
			increaseCount++
		}
		lastNumber = lines[i]
	}
	return fmt.Sprintf("1: Count of numbers which are bigger than their predecessor %d", increaseCount)
}

func task2() string {
	lines, err := readLines("./day1/input.txt")

	handleError(err)
	increaseCount := 0
	lastNumber := 0

	for i := 0; i < len(lines)-3; i++ {
		windowSum := lines[i] + lines[i+1] + lines[i+2]
		if windowSum > lastNumber {
			increaseCount++
		}
		lastNumber = windowSum
	}
	return fmt.Sprintf("2: Count of search windows which are bigger than their predecessor %d", increaseCount)
}

func main() {
	println(task1())
	println(task2())
}
