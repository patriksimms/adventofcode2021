package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

// copied from https://stackoverflow.com/a/18479916/13130130
// readLines reads a whole file into memory
// and returns a slice of its lines.
func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		value := scanner.Text()
		lines = append(lines, value)
	}
	return lines, scanner.Err()
}

func handleError(err error) {
	if err != nil {
		log.Fatalf("readLines: %s", err)
	}
}

func task1() string {
	lines, err := readLines("./day2/input.txt")

	handleError(err)

	horizontalPosition := 0
	depth := 0

	for i := 0; i < len(lines); i++ {
		strNum := lines[i][len(lines[i])-1:]
		num, err := strconv.Atoi(strNum)
		handleError(err)
		if strings.HasPrefix(lines[i], "forward") {
			horizontalPosition += num
		} else if strings.HasPrefix(lines[i], "down") {
			depth += num
		} else if strings.HasPrefix(lines[i], "up") {
			depth -= num
		}
	}
	return fmt.Sprintf("1: Result: %d", depth*horizontalPosition)
}

func task2() string {
	lines, err := readLines("./day2/input.txt")

	handleError(err)

	horizontalPosition := 0
	depth := 0
	aim := 0

	for i := 0; i < len(lines); i++ {
		strNum := lines[i][len(lines[i])-1:]
		num, err := strconv.Atoi(strNum)
		handleError(err)
		if strings.HasPrefix(lines[i], "forward") {
			depth += num
			horizontalPosition += num * aim
		} else if strings.HasPrefix(lines[i], "down") {
			aim += num
		} else if strings.HasPrefix(lines[i], "up") {
			aim -= num
		}
	}
	return fmt.Sprintf("1: Result: %d", depth*horizontalPosition)
}

func main() {
	println(task1())
	println(task2())
}
